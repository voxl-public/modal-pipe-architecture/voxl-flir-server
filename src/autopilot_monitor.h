
#ifndef AUTOPILOT_MONITOR_H
#define AUTOPILOT_MONITOR_H

void connect_to_autopilot(void);

void disconnect_from_autopilot(void);


// flow mode only runs shutter when NOT flying pos mode
int autopilot_is_not_in_position_mode(void);

#endif // AUTOPILOT_MONITOR_H
