

#include <stdio.h>
#include "common.h"
#include "autopilot_monitor.h"
#include <c_library_v2/common/mavlink.h>
#include <modal_pipe.h>
#include "config_file.h"

// slow data rate pipe, only need heartbeats
#define MAVLINK_PIPE	"mavlink_to_gcs"
#define MAV_PIPE_CH		0

typedef enum px4_main_mode{
    PX4_MAIN_MODE_UNKNOWN = 0,
    PX4_MAIN_MODE_MANUAL = 1,
    PX4_MAIN_MODE_ALTCTL = 2,
    PX4_MAIN_MODE_POSCTL = 3,
    PX4_MAIN_MODE_AUTO = 4,
    PX4_MAIN_MODE_ACRO = 5,
    PX4_MAIN_MODE_OFFBOARD = 6,
    PX4_MAIN_MODE_STABILIZED = 7,
    PX4_MAIN_MODE_RATTITUDE = 8
} px4_main_mode;

typedef enum px4_sub_mode{
    PX4_SUB_MODE_UNKNOWN = 0,
    PX4_SUB_MODE_AUTO_READY = 1,
    PX4_SUB_MODE_AUTO_TAKEOFF = 2,
    PX4_SUB_MODE_AUTO_LOITER = 3,
    PX4_SUB_MODE_AUTO_MISSION = 4,
    PX4_SUB_MODE_AUTO_RTL = 5,
    PX4_SUB_MODE_AUTO_LAND = 6,
    PX4_SUB_MODE_AUTO_RTGS = 7,
    PX4_SUB_MODE_AUTO_FOLLOW_TARGET = 8,
    PX4_SUB_MODE_AUTO_PRECLAND = 9
} px4_sub_mode;


static int has_opened_connection_to_autopilot = 0;
static int has_received_heartbeat = 0;
static int is_armed = 0;
static int is_pos_mode = 0;


static px4_main_mode _ap_monitor_get_main_mode(uint32_t heartbeat_custom_mode)
{
	return (heartbeat_custom_mode&0x00FF0000)>>16;
}


static px4_sub_mode _ap_monitor_get_sub_mode(uint32_t heartbeat_custom_mode)
{
	return (heartbeat_custom_mode&0xFF000000)>>24;
}



static void _autopilot_helper_cb(__attribute__((unused))int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets, i;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL){
		return;
	}

	for(i=0; i<n_packets; i++){
		mavlink_message_t* msg = &msg_array[i];
		if(msg->msgid == MAVLINK_MSG_ID_HEARTBEAT){

			if(!has_received_heartbeat) printf("received first heartbeat from autopilot\n");
			has_received_heartbeat = 1;
			if(en_debug) printf("parsing heartbeat messages from autopilot\n");

			//uint32_t heartbeat_system_status = mavlink_msg_heartbeat_get_system_status(msg);
			uint8_t heartbeat_base_mode     = mavlink_msg_heartbeat_get_base_mode(msg);
			uint32_t heartbeat_custom_mode   = mavlink_msg_heartbeat_get_custom_mode(msg);

			if(_ap_monitor_get_main_mode(heartbeat_custom_mode)==PX4_MAIN_MODE_POSCTL){
				if(!is_pos_mode) printf("detected PX4 going into position hold mode\n");
				is_pos_mode = 1;
			}else{
				if(is_pos_mode) printf("detected PX4 going out of position hold mode\n");
				is_pos_mode = 0;
			}

			int new_is_armed = heartbeat_base_mode & MAV_MODE_FLAG_SAFETY_ARMED;

			if(new_is_armed && !is_armed) printf("detected PX4 just armed\n");
			else if(!new_is_armed && is_armed) printf("detected PX4 just disarmed\n");
			is_armed = new_is_armed;
		}
	}

	return;
}

void connect_to_autopilot(void)
{
	if(has_opened_connection_to_autopilot) return;
	printf("opening mavlink pipe to listen to autopilot\n");
	pipe_client_set_simple_helper_cb(MAV_PIPE_CH, _autopilot_helper_cb, NULL);
	pipe_client_open(MAV_PIPE_CH, MAVLINK_PIPE, PROCESS_NAME, \
					EN_PIPE_CLIENT_SIMPLE_HELPER, sizeof(mavlink_message_t));
	has_opened_connection_to_autopilot = 1;
	return;
}


void disconnect_from_autopilot(void)
{
	if(! has_opened_connection_to_autopilot) return;
	pipe_client_close(MAV_PIPE_CH);
	has_opened_connection_to_autopilot = 0;
	return;
}




// flow mode runs the shutter in position mode, and goes more quickly while waiting
// if we are ever uncertain, still run auto shutter by reporting position mode
int autopilot_is_not_in_position_mode(void)
{
	/*
	if(! has_opened_connection_to_autopilot) printf("has not opened connection\n");
	if(!_ap_monitor_is_armed()) printf("is not armed\n");
	if(_ap_monitor_get_main_mode()!=PX4_MAIN_MODE_POSCTL) printf("is_not_pos_mode\n");
	*/

	// yes I could make this one big if statement but we may add extra
	// conditions in the future and this is easier to read if it gets longer.
	if(! has_opened_connection_to_autopilot) return 0;
	if(! has_received_heartbeat) return 0;
	if(! is_armed) return 1;
	if(is_pos_mode) return 0;

	// default to assuming we are in pos mode which is normal auto shutter
	return 0;
}
