/*******************************************************************************
 * Copyright 2022 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#ifndef MODAL_LEPTON_COMMON_H
#define MODAL_LEPTON_COMMON_H

#include <stdint.h>

#define PROCESS_NAME          "voxl-lepton-server" // name of pid file
#define FRAME_WIDTH           160
#define FRAME_HEIGHT          120
#define FRAME_SIZE            FRAME_WIDTH * FRAME_HEIGHT



#define VOSPI_PACKET_SIZE     164 // VoSPI packet size bytes
#define VOSPI_PACKET_SIZE_16  VOSPI_PACKET_SIZE / 2 // VoSPI packet 16bit size
#define PACKETS_PER_SEGMENT   60
#define SEGMENT_SIZE          VOSPI_PACKET_SIZE * PACKETS_PER_SEGMENT
#define RAW_FRAME_SIZE        SEGMENT_SIZE * 4  // Size of raw frame
#define RAW_FRAME_SIZE_16     RAW_FRAME_SIZE / 2 // size of raw frame 16bit


// TODO tune this for APQ
// 100ms based on flow testing vs gyro
#define READ_DELAY_NS   100000000
#define ODR_HZ          8.77




#define SHUTTER_MODE_STRINGS {"auto","manual","flow"}
typedef enum shutter_mode_t{
	SHUTTER_MODE_AUTO,
	SHUTTER_MODE_MANUAL,
	SHUTTER_MODE_FLOW
}shutter_mode_t;

#define COMMAND_SET_FFC_SHUTTER_AUTO	"set_ffc_shutter_auto"
#define COMMAND_SET_FFC_SHUTTER_MANUAL	"set_ffc_shutter_manual"
#define COMMAND_SET_FFC_SHUTTER_FLOW	"set_ffc_shutter_flow"
#define COMMAND_RUN_FFC_SHUTTER			"run_ffc_shutter"
#define COMMAND_REBOOT_LEPTON			"reboot_lepton"

#define CONTROL_COMMANDS (	COMMAND_SET_FFC_SHUTTER_AUTO "," \
							COMMAND_SET_FFC_SHUTTER_MANUAL "," \
							COMMAND_SET_FFC_SHUTTER_FLOW "," \
							COMMAND_RUN_FFC_SHUTTER "," \
							COMMAND_REBOOT_LEPTON)

typedef struct lepton_log_packet{
	uint8_t packet_version;
	int64_t timestamp_ns;

	float aux_temp;
	float fpa_temp;

	double time_since_shutter;

	uint16_t reboot_cnt;
	uint16_t sync_cnt;
} __attribute((packed))__lepton_log_packet;


#define TIMING_PACKET_CNT 10

typedef struct lepton_timing_packet{
	uint8_t packet_version;

	int64_t time_start_read_ns[TIMING_PACKET_CNT];
	int64_t time_stop_read_ns[TIMING_PACKET_CNT];

	uint8_t try_sync[TIMING_PACKET_CNT];
	uint8_t published[TIMING_PACKET_CNT];
} __attribute((packed))__lepton_timing_packet;


uint16_t global_reboot_cnt;
uint16_t global_sync_cnt;

#endif
